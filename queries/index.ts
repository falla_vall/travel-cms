import { gql } from "@apollo/client";

export const QUERIES_TRAVEL_FILES = gql`
  query GetTravelFiles($id: ID!) {
    travelPackage(id: $id) {
      data {
        id
        attributes {
          title
          description
          price
          img {
            data {
              id
              attributes {
                name
                url
              }
            }
          }
        }
      }
    }
    uploadFiles {
      data {
        id
        attributes {
          name
          url
        }
      }
    }
  }
`;

export const QUERIES_TRAVEL = gql`
  query GetTravel($id: ID!) {
    travelPackage(id: $id) {
      data {
        id
        attributes {
          title
          description
          price
          img {
            data {
              id
              attributes {
                name
                url
              }
            }
          }
        }
      }
    }
  }
`;

export const QUERIES_ORDER = gql`
  query GetOrder($id: ID!) {
    order(id: $id) {
      data {
        id
        attributes {
          totalPrice
          createdAt
          customer {
            data {
              id
              attributes {
                name
              }
            }
          }
          order_details {
            data {
              id
              attributes {
                price
                travel_package {
                  data {
                    id
                    attributes {
                      title
                      img {
                        data {
                          attributes {
                            url
                          }
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const QUERIES_TRAVELS_CUSTOMERS = gql`
  query {
    travelPackages {
      data {
        id
        attributes {
          title
          description
          price
          img {
            data {
              id
              attributes {
                name
                url
              }
            }
          }
        }
      }
    }
    customers {
      data {
        id
        attributes {
          name
          email
        }
      }
    }
  }
`;

export const QUERIES_TRAVELS = gql`
  query {
    travelPackages {
      data {
        id
        attributes {
          title
          description
          price
          img {
            data {
              attributes {
                url
              }
            }
          }
        }
      }
    }
  }
`;

export const QUERIES_CUSTOMERS = gql`
  query {
    customers {
      data {
        id
        attributes {
          name
          email
          phone
          address
        }
      }
    }
  }
`;

export const QUERIES_ORDERS = gql`
  query {
    orders {
      data {
        id
        attributes {
          totalPrice
          createdAt
          customer {
            data {
              id
              attributes {
                name
              }
            }
          }
          order_details {
            data {
              id
              attributes {
                price
                travel_package {
                  data {
                    id
                    attributes {
                      title
                    }
                  }
                }
              }
            }
          }
        }
      }
    }
  }
`;

export const UPLOAD_FILE = gql`
  mutation UploadFile($file: Upload!) {
    upload(file: $file) {
      data {
        id
      }
    }
  }
`;

export const CREATE_TRAVEL = gql`
  mutation CreateTravel(
    $title: String
    $description: String
    $price: Int
    $img: ID
  ) {
    createTravelPackage(
      data: {
        title: $title
        description: $description
        price: $price
        img: $img
      }
    ) {
      data {
        id
        attributes {
          title
          description
          price
          img {
            data {
              id
              attributes {
                name
                url
              }
            }
          }
        }
      }
    }
  }
`;

export const CREATE_CUSTOMER = gql`
  mutation CreateCustomer(
    $name: String
    $email: String
    $phone: String
    $address: String
  ) {
    createCustomer(
      data: { name: $name, email: $email, phone: $phone, address: $address }
    ) {
      data {
        id
        attributes {
          name
          email
          phone
          address
        }
      }
    }
  }
`;

export const UPDATE_TRAVEL = gql`
  mutation UpdateTravel(
    $id: ID!
    $title: String
    $description: String
    $price: Int
    $img: ID
  ) {
    updateTravelPackage(
      id: $id
      data: {
        title: $title
        description: $description
        price: $price
        img: $img
      }
    ) {
      data {
        id
        attributes {
          title
          description
          price
          img {
            data {
              id
              attributes {
                name
                url
              }
            }
          }
        }
      }
    }
  }
`;

export const UPDATE_CUSTOMER = gql`
  mutation UpdateCustomer(
    $id: ID!
    $name: String
    $email: String
    $phone: String
    $address: String
  ) {
    updateCustomer(
      id: $id
      data: { name: $name, email: $email, phone: $phone, address: $address }
    ) {
      data {
        id
        attributes {
          name
          email
          phone
          address
        }
      }
    }
  }
`;

export const CREATE_ORDER = gql`
  mutation CreateOrder($customer: ID, $totalPrice: Int, $order_details: [ID]) {
    createOrder(
      data: {
        customer: $customer
        totalPrice: $totalPrice
        order_details: $order_details
      }
    ) {
      data {
        id
      }
    }
  }
`;

export const CREATE_ORDER_DETAIL = gql`
  mutation CreateOrderDetail($order: ID, $travel_package: ID, $price: Int) {
    createOrderDetail(
      data: { order: $order, travel_package: $travel_package, price: $price }
    ) {
      data {
        id
      }
    }
  }
`;

export const DELETE_TRAVEL = gql`
  mutation DeleteTravel($id: ID!) {
    deleteTravelPackage(id: $id) {
      data {
        id
        attributes {
          title
          description
          price
          img {
            data {
              attributes {
                url
              }
            }
          }
        }
      }
    }
  }
`;

export const DELETE_CUSTOMER = gql`
  mutation DeleteCustomer($id: ID!) {
    deleteCustomer(id: $id) {
      data {
        id
      }
    }
  }
`;

export const DELETE_ORDER = gql`
  mutation DeleteOrder($id: ID!) {
    deleteOrder(id: $id) {
      data {
        id
      }
    }
  }
`;

export const QUERIES_FILES = gql`
  query {
    uploadFiles {
      data {
        id
        attributes {
          name
          url
        }
      }
    }
  }
`;

export const DELETE_FILE = gql`
  mutation DeleteFile($id: ID!) {
    deleteFile(id: $id) {
      data {
        id
        attributes {
          name
          url
        }
      }
    }
  }
`;

export const LOGIN = gql`
  mutation Login($email: String!, $password: String!) {
    login(input: { identifier: $email, password: $password }) {
      jwt
      user {
        id
        username
        email
      }
    }
  }
`;

export const REGISTER = gql`
  mutation Register($username: String!, $email: String!, $password: String!) {
    register(
      input: { username: $username, email: $email, password: $password }
    ) {
      jwt
      user {
        id
        username
        email
      }
    }
  }
`;
