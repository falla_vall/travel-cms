import React from "react";
import Head from "next/head";
import Image from "next/image";
import { useRecoilState } from "recoil";
import Navbar from "./ui/Navbar";
import Modal from "./ui/Modal";
import { ChildrenProps } from "../types";
import { loadingState } from "../atoms";
import Loading from "./ui/Loading";

const Layout: React.FC<ChildrenProps> = ({ children }) => {
  const [loading, setLoading] = useRecoilState(loadingState);
  return (
    <>
      <Head>
        <title>Travel CMS</title>
        <meta name="description" content="Travel CMS" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <div className="flex flex-row font-poppins">
        <Navbar />
        <Loading visState={[loading, setLoading]} />
        <main className="py-12 container overflow-y-auto text-sky-900">
          {children}
        </main>
      </div>
    </>
  );
};

export default Layout;
