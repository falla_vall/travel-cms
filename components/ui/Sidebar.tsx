import React, { useEffect, useState } from "react";
import Image from "next/image";
import { useMutation } from "@apollo/client";
import { useRecoilState } from "recoil";
import { Dialog } from "@headlessui/react";
import toast from "react-hot-toast";
import Modal from "./Modal";
import { SidebarProps } from "../../types";
import { loadingState } from "../../atoms";
import { CREATE_ORDER, CREATE_ORDER_DETAIL } from "../../queries";
import DropUp from "./DropUp";

const Sidebar: React.FC<SidebarProps> = ({
  visState,
  selectedsState,
  customers,
}) => {
  const [selecteds, dispatch] = selectedsState;
  const [selectedCustomer, setSelectedCustomer] = useState(customers[0]);
  const [loading, setLoading] = useRecoilState(loadingState);
  const [price, setPrice] = useState(0);

  const [createOrder, optionsOrder] = useMutation(CREATE_ORDER);
  const [createOrderDetail, optionsOrderDetail] =
    useMutation(CREATE_ORDER_DETAIL);

  const handleSubmit = () => {
    const promise = createOrder({
      variables: {
        customer: selectedCustomer.id,
        totalPrice: price,
      },
    }).then((res) => {
      const order = res.data.createOrder.data.id;
      selecteds.forEach((selected) => {
        createOrderDetail({
          variables: {
            order,
            travel_package: selected.id,
            price: selected.attributes.price,
          },
        }).then(() => dispatch({ type: "REMOVE_SELECTED", payload: selected }));
      });
    });
    toast.promise(promise, {
      loading: "Creating order...",
      success: "Order created successfully",
      error: "Error creating order",
    });
  };

  useEffect(() => {
    if (optionsOrder.loading || optionsOrderDetail.loading) {
      setLoading(true);
    } else {
      setLoading(false);
    }
  }, [optionsOrder, optionsOrderDetail]); // eslint-disable-line

  useEffect(() => {
    setPrice(
      selecteds.reduce((total, item) => total + item.attributes.price, 0)
    );
  }, [selecteds]);
  return (
    <Modal visState={visState} position="right">
      <Dialog.Panel className="h-screen max-h-screen w-screen-1/4 flex flex-col justify-between bg-white text-left p-8">
        <div className="space-y-4">
          <Dialog.Title as="h3">
            <span className="text-xl font-medium leading-6 text-gray-900">
              Orders
            </span>
          </Dialog.Title>
          <div className="flex flex-col py-2 gap-2 max-h-100 overflow-y-auto">
            {selecteds.length > 0 ? (
              selecteds.map((selected, index) => (
                <div
                  key={index}
                  className="flex items-center p-2 gap-4 border border-gray-300 shadow rounded-lg"
                >
                  <div className="basis-1/4 relative aspect-square bg-white rounded-lg overflow-clip">
                    <Image
                      src={selected.attributes.img.data.attributes.url}
                      alt="image"
                      layout="fill"
                      objectFit="cover"
                    />
                  </div>
                  <div className="flex flex-col basis-3/4 text-sky-900">
                    <span className="text-xl">{selected.attributes.title}</span>
                    <span className="text-xs font-thin">
                      {new Intl.NumberFormat("en-US", {
                        style: "currency",
                        currency: "IDR",
                      }).format(selected.attributes.price)}
                    </span>
                  </div>
                </div>
              ))
            ) : (
              <div className="flex items-center justify-center py-12">
                <span className="text-xl">No selected</span>
              </div>
            )}
          </div>
        </div>
        <div className="flex flex-col items-end gap-4">
          <span className="w-full">Customer</span>
          <DropUp
            selectedState={[selectedCustomer, setSelectedCustomer]}
            options={customers}
          />
          <div className="flex justify-between w-full">
            <div className="text-xl font-semibold">Total</div>
            <div className="text-xl">
              {new Intl.NumberFormat("en-US", {
                style: "currency",
                currency: "IDR",
              }).format(price)}
            </div>
          </div>
          <button
            disabled={selecteds.length === 0}
            onClick={() => handleSubmit()}
            className="px-4 py-2 rounded-lg bg-sky-500 text-white hover:(bg-sky-600) disabled:(bg-sky-900 text-gray-400 cursor-not-allowed) transition-colors duration-200"
          >
            Order Now
          </button>
        </div>
      </Dialog.Panel>
    </Modal>
  );
};

export default Sidebar;
