import React, { useEffect, useState } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { signOut, useSession } from "next-auth/react";
import { AiOutlineDashboard } from "react-icons/ai";
import {
  MdPeopleOutline,
  MdOutlineDashboard,
  MdOutlineLogout,
  MdMenu,
  MdCardTravel,
  MdMenuOpen,
} from "react-icons/md";

const icons = [
  {
    name: "Dashboard",
    icon: <AiOutlineDashboard size={20} />,
    url: "/dashboard",
  },
  {
    name: "Travel",
    icon: <MdCardTravel size={20} />,
    url: "/travel",
  },
  {
    name: "Customer",
    icon: <MdPeopleOutline size={20} />,

    url: "/customer",
  },
  {
    name: "Order",
    icon: <MdOutlineDashboard size={20} />,
    url: "/order",
  },
];

const Navbar: React.FC = () => {
  const router = useRouter();
  const [isOpen, setIsOpen] = useState(false);

  const { data: session } = useSession();

  useEffect(() => {
    if (!session) {
      router.push("/auth");
    }
  }, [session]); // eslint-disable-line

  return (
    <div
      className={`${
        isOpen ? "w-56" : "w-14"
      } sticky top-0 flex flex-col justify-around items-center h-screen px-8 group transform-gpu transition-all duration-300 ease-in-out bg-white`}
    >
      <button
        onClick={() => setIsOpen((prev) => !prev)}
        className={`${
          isOpen ? "w-52 justify-start gap-4 px-4" : "w-12 justify-center"
        } h-12 flex items-center rounded-lg hover:(bg-sky-100 text-sky-500) transition-all duration-200 text-gray-400`}
      >
        {isOpen ? <MdMenuOpen size={20} /> : <MdMenu size={20} />}
        <span className={isOpen ? "block" : "hidden"}>Menu</span>
      </button>
      <div className="flex flex-col gap-2">
        {icons.map((icon, index) => (
          <Link key={index} href={icon.url}>
            <a
              className={`${
                isOpen ? "w-52 justify-start gap-4 px-4" : "w-12 justify-center"
              } h-12 flex items-center rounded-lg hover:(bg-sky-100 text-sky-500) transition-all duration-200 ${
                router.pathname.startsWith(icon.url)
                  ? "bg-sky-200 text-sky-600"
                  : "text-gray-400"
              }`}
            >
              {icon.icon}
              <span className={isOpen ? "block" : "hidden"}>{icon.name}</span>
            </a>
          </Link>
        ))}
      </div>
      <button
        onClick={() => signOut()}
        className={`${
          isOpen ? "w-52 justify-start gap-4 px-4" : "w-12 justify-center"
        } h-12 flex items-center rounded-lg hover:(bg-sky-100 text-sky-500) transition-all duration-200 text-gray-400`}
      >
        <MdOutlineLogout size={20} />
        <span className={isOpen ? "block" : "hidden"}>Log out</span>
      </button>
    </div>
  );
};

export default Navbar;
