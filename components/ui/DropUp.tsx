import React, { useEffect, useRef, useState } from "react";
import { MdKeyboardArrowUp } from "react-icons/md";
import { DropUpProps } from "../../types";

const DropUp: React.FC<DropUpProps> = ({ selectedState, options }) => {
  const dropupRef = useRef<HTMLDivElement>(null);
  const buttonRef = useRef<HTMLButtonElement>(null);
  const [selected, setSelected] = selectedState;
  const [isOpen, setIsOpen] = useState(false);

  useEffect(() => {
    const handleClickOutside = (e: MouseEvent) => {
      if (
        dropupRef.current &&
        !dropupRef.current.contains(e.target as Node) &&
        buttonRef.current &&
        !buttonRef.current.contains(e.target as Node)
      ) {
        setIsOpen(false);
      }
    };
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, [dropupRef]);

  useEffect(() => {
    if (selected) {
      setIsOpen(false);
    }
  }, [selected]);
  return (
    <div className="relative w-full inline-block">
      <button
        ref={buttonRef}
        onClick={() => setIsOpen(!isOpen)}
        className={`flex justify-between items-center w-full px-4 py-2 rounded-lg border border-gray-300 shadow-xl ${
          isOpen && "ring ring-sky-500"
        }`}
      >
        <div className="text-lg">{selected.attributes.name}</div>
        <MdKeyboardArrowUp
          size={20}
          className={`text-gray-400 transform-gpu transition-all duration-200 ${
            isOpen ? "rotate-0" : "rotate-180"
          }`}
        />
      </button>
      <div
        ref={dropupRef}
        className={`${
          isOpen ? "block" : "hidden"
        } absolute bottom-12 inset-x-0 w-full max-h-52 overflow-y-auto bg-white rounded-lg shadow-2xl overflow-clip`}
      >
        {options.map((option, index) => (
          <div
            onClick={() => setSelected(option)}
            key={index}
            className="w-full cursor-pointer px-4 py-2 hover:(bg-gray-100)"
          >
            {option.attributes.name}
          </div>
        ))}
      </div>
    </div>
  );
};

export default DropUp;
