import React, { useRef, useState } from "react";
import Image from "next/image";
import { Dialog } from "@headlessui/react";
import { useRecoilState } from "recoil";
import toast from "react-hot-toast";
import { BiImageAdd } from "react-icons/bi";
import { loadingState } from "../../atoms";
import { FilesModalProps } from "../../types";
import Modal from "./Modal";

const FilesModal: React.FC<FilesModalProps> = ({
  visState,
  previewState,
  files,
}) => {
  const [_isOpen, setIsOpen] = visState;
  const [_preview, setPreview] = previewState;
  const [data, setData] = useState(files);
  const [_loading, setLoading] = useRecoilState(loadingState);
  const imgRef = useRef<HTMLInputElement>(null);

  const handleUpload = (e: React.ChangeEvent<HTMLInputElement>) => {
    const file = e.target.files?.[0];
    if (!file) return;
    setLoading(true);
    const formData = new FormData();
    const url = "https://travel-cuy.herokuapp.com/api/upload";
    formData.append("files", file);
    const promise = fetch(url, {
      method: "POST",
      body: formData,
    })
      .then((resp) => resp.json())
      .then((res) => {
        setLoading(true);
        setData([
          ...data,
          {
            id: res[0].id,
            attributes: {
              name: res[0].name,
              url: res[0].url,
            },
          },
        ]);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      })
      .catch((err) => {
        console.log(err);
        setLoading(false);
      });

    toast
      .promise(promise, {
        loading: "Uploading...",
        success: "Uploaded successfully",
        error: "Error uploaded",
      })
      .then(() => {
        setLoading(false);
      });
  };

  return (
    <Modal visState={visState}>
      <Dialog.Panel className="w-full max-w-4xl transform overflow-hidden rounded-2xl bg-white p-6 text-left align-middle shadow-xl transition-all">
        <Dialog.Title
          as="h3"
          className="text-lg font-medium leading-6 text-gray-900"
        >
          Choose an Image
        </Dialog.Title>
        <div className="grid grid-cols-3 gap-4">
          {data.map((file, index) => (
            <div
              key={index}
              onClick={() => {
                setPreview(file);
                setIsOpen(false);
              }}
              className="relative cursor-pointer overflow-clip aspect-video rounded-lg border-2 border-gray-300 group"
            >
              <Image
                src={file.attributes.url}
                alt="image"
                layout="fill"
                objectFit="cover"
                className="group-hover:(scale-102) transform transition-all duration-200"
              />
            </div>
          ))}
          <div
            onClick={() => imgRef.current?.click()}
            className="flex justify-center items-center relative cursor-pointer overflow-clip aspect-video rounded-lg border-2 border-gray-300 hover:(scale-102) transform transition-all duration-200"
          >
            <BiImageAdd size={64} className="text-gray-300" />
          </div>
          <input
            ref={imgRef}
            type="file"
            name="image"
            id="image"
            onChange={handleUpload}
            hidden
          />
        </div>
      </Dialog.Panel>
    </Modal>
  );
};

export default FilesModal;
