import React, { useEffect } from "react";
import { useRouter } from "next/router";
import { useSession } from "next-auth/react";
import { WelcomeProps } from "../../types";

const Welcome: React.FC<WelcomeProps> = ({ desc }) => {
  const router = useRouter();
  const { data: session } = useSession();
  useEffect(() => {
    if (!session) {
      router.push("/auth");
    }
  });
  return (
    <>
      <h1 className="text-4xl">
        Welcome <span className="text-xl">{session?.user.email}</span> 👋
      </h1>
      <p className="text-md pt-4">{desc}</p>
    </>
  );
};

export default Welcome;
