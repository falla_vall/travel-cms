import React, { useEffect, useState } from "react";
import { Dialog } from "@headlessui/react";
import toast from "react-hot-toast";
import { useMutation } from "@apollo/client";
import { useRecoilState } from "recoil";
import Modal from "./Modal";
import { loadingState } from "../../atoms";
import { CREATE_CUSTOMER } from "../../queries";
import { CreateModalProps } from "../../types";

const CreateModal: React.FC<CreateModalProps> = ({ visState, setData }) => {
  const initialForm = {
    name: "",
    email: "",
    phone: "",
    address: "",
  };
  const [createCustomer, options] = useMutation(CREATE_CUSTOMER);
  const [form, setForm] = useState(initialForm);
  const [_loading, setLoading] = useRecoilState(loadingState);
  const [isOpen, setIsOpen] = visState;

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    const { name, value } = e.target;
    setForm({ ...form, [name]: value });
  };

  const handleSubmit = () => {
    toast.promise(
      createCustomer({ variables: form }).then((res) => {
        setData((prev) => [...prev, res.data.createCustomer.data]);
        setForm(initialForm);
      }),
      {
        loading: "Creating customer...",
        success: "Customer created successfully",
        error: "Error creating customer",
      }
    );
  };

  useEffect(() => {
    if (options.loading) {
      setLoading(true);
    } else {
      setLoading(false);
    }
    if (options.error) {
      console.log({ error: options.error });
    }
  }, [options]); // eslint-disable-line
  return (
    <Modal visState={[isOpen, setIsOpen]}>
      <Dialog.Panel className="w-full max-w-md transform overflow-hidden rounded-lg bg-white p-6 text-left align-middle shadow-xl transition-all">
        <Dialog.Title
          as="h3"
          className="text-xl font-medium leading-6 text-gray-900"
        >
          Create a New
        </Dialog.Title>
        <div className="py-2">
          {Object.keys(form).map((key, index) => (
            <div key={index} className="py-2">
              <label htmlFor={key} className="font-thin text-sm">
                {key.toUpperCase()}
              </label>
              <input
                type="text"
                name={key}
                id={key}
                value={form[key]}
                onChange={handleChange}
                className="w-full border border-gray-200 px-4 py-2 rounded-lg focus:(outline-none ring focus-ring:border-blue-500)"
              />
            </div>
          ))}
        </div>
        <button
          onClick={() => handleSubmit()}
          className="w-full mt-6 py-2 rounded-lg bg-sky-500 text-white hover:(bg-sky-600 text-gray-200) transition-colors duration-200"
        >
          Submit
        </button>
      </Dialog.Panel>
    </Modal>
  );
};

export default CreateModal;
