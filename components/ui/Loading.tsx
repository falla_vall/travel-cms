import React from "react";
import Image from "next/image";
import Modal from "./Modal";
import { LoadingProps } from "../../types";

const Loading: React.FC<LoadingProps> = ({ visState }) => {
  const [loading, setLoading] = visState;
  return (
    <Modal visState={[loading, setLoading]}>
      <div className="flex items-center justify-center">
        <Image
          src="/loading.gif"
          alt="Loading"
          width={96}
          height={96}
          objectFit="contain"
        />
      </div>
    </Modal>
  );
};

export default Loading;
