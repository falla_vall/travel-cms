import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import toast from "react-hot-toast";
import { useRecoilState } from "recoil";
import { AiFillEdit, AiFillDelete, AiFillSave } from "react-icons/ai";
import { loadingState } from "../../atoms";
import { TableProps } from "../../types";

const Table: React.FC<TableProps> = ({
  data,
  columns,
  updMutation,
  delMutation,
  disableEdit,
}) => {
  const router = useRouter();

  const [items, setItems] = useState(data);
  const [loading, setLoading] = useRecoilState(loadingState);
  const [updateMutation, updateOptions] = updMutation || [null, null];
  const [deleteMutation, deleteOptions] = delMutation;
  const [editForm, setEditForm] = useState({
    id: null,
    attributes: {},
  });

  const handleEdit = (id: number) => {
    toast
      .promise(
        updateMutation({
          variables: {
            id,
            ...editForm.attributes,
          },
        }),
        {
          loading: "Updating...",
          success: "Updated successfully",
          error: "Error updating",
        }
      )
      .then(() => {
        setItems(items.map((item) => (item.id === id ? editForm : item)));
        setEditForm({
          id: null,
          attributes: {},
        });
      });
  };

  const handleDelete = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    id: number
  ) => {
    e.stopPropagation();
    toast.promise(
      deleteMutation({
        variables: { id },
      }).then(() => {
        setItems(items.filter((item) => item.id !== id));
      }),
      {
        loading: "Deleting...",
        success: "Deleted successfully",
        error: "Error deleting",
      }
    );
  };

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setEditForm({
      ...editForm,
      attributes: {
        ...editForm.attributes,
        [e.target.name.toLowerCase()]: e.target.value,
      },
    });
  };

  useEffect(() => {
    if (deleteOptions.loading || updateOptions?.loading) {
      setLoading(true);
    } else {
      setLoading(false);
    }
    if (deleteOptions.error || updateOptions?.error) {
      console.log(
        updateOptions?.error
          ? { error: updateOptions.error }
          : { error: deleteOptions.error }
      );
    }
  }, [deleteOptions, updateOptions]); // eslint-disable-line

  useEffect(() => {
    setItems(data);
  }, [data]);

  return (
    <div className="overflow-x-auto relative rounded-lg">
      <table className="w-full text-sm text-left text-gray-500">
        <thead className="text-xs text-sky-700 uppercase bg-sky-200">
          <tr>
            {columns.map((column, index) => (
              <th key={index} scope="col" className="py-3 px-6">
                {column}
              </th>
            ))}
            <th scope="col" className="py-3 px-6">
              action
            </th>
          </tr>
        </thead>
        <tbody>
          {items.map((row, index) => (
            <tr
              key={index}
              onClick={() =>
                disableEdit ? router.push(`/order/${row.id}`) : null
              }
              className="bg-white border-b hover:(bg-gray-100) transition-colors duration-200"
            >
              {columns.map((column, i) =>
                i < 1 ? (
                  <th
                    key={i}
                    scope="row"
                    className="py-4 px-6 font-medium text-gray-900 whitespace-nowrap"
                  >
                    {row.id}
                  </th>
                ) : (
                  <td key={i} className="py-4 px-6">
                    {editForm.id === row.id ? (
                      <input
                        type="text"
                        name={column}
                        value={editForm.attributes[column]}
                        onChange={handleChange}
                        className="border border-yellow-500 rounded-lg"
                      />
                    ) : (
                      row.attributes[column]
                    )}
                  </td>
                )
              )}
              <td className="flex gap-2 py-2 px-6">
                {editForm.id === row.id ? (
                  <button
                    onClick={() => handleEdit(row.id)}
                    className="flex items-center justify-center p-2 border-2 border-green-500 rounded-lg shadow-sm shadow-green-500 text-green-500 hover:(bg-green-500 text-white) transition-colors duration-200"
                  >
                    <AiFillSave size={16} />
                  </button>
                ) : (
                  !disableEdit && (
                    <button
                      onClick={() => setEditForm(row)}
                      className="flex items-center justify-center p-2 border-2 border-yellow-500 rounded-lg shadow-sm shadow-yellow-500 text-yellow-500 hover:(bg-yellow-500 text-white) transition-colors duration-200"
                    >
                      <AiFillEdit size={16} />
                    </button>
                  )
                )}
                <button
                  onClick={(e) => handleDelete(e, row.id)}
                  className="flex items-center justify-center p-2 border-2 border-red-500 rounded-lg shadow-sm shadow-red-500 text-red-500 hover:(bg-red-500 text-white) transition-colors duration-200"
                >
                  <AiFillDelete size={16} />
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};

export default Table;
