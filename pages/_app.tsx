import "windi.css";
import "../styles/globals.css";
import type { AppProps } from "next/app";
import NextNProgress from "nextjs-progressbar";
import { SessionProvider } from "next-auth/react";
import { ApolloProvider } from "@apollo/client";
import { RecoilRoot } from "recoil";
import { Toaster } from "react-hot-toast";
import client from "../lib/client";
import Layout from "../components/Layout";

function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  return (
    <SessionProvider session={session}>
      <ApolloProvider client={client}>
        <RecoilRoot>
          <Layout>
            <NextNProgress />
            <Component {...pageProps} />
          </Layout>
          <Toaster />
        </RecoilRoot>
      </ApolloProvider>
    </SessionProvider>
  );
}

export default MyApp;
