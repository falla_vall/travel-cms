import type { NextPage, GetServerSideProps } from "next";
import { useEffect } from "react";
import { useRouter } from "next/router";
import { getSession } from "next-auth/react";
import Image from "next/image";

const Home: NextPage = () => {
  const router = useRouter();
  useEffect(() => {
    router.push("/dashboard");
  }, [router]);
  return (
    <div className="flex justify-center items-center w-full h-full">
      <Image
        src="/loading.gif"
        alt="loading"
        width={96}
        height={96}
        objectFit="contain"
      />
    </div>
  );
};

export default Home;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);
  // Check if session exists or not, if not, redirect
  if (session == null) {
    return {
      redirect: {
        destination: "/auth",
        permanent: true,
      },
    };
  }
  return {
    props: {},
  };
};
