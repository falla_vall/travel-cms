import type { NextPage, GetServerSideProps } from "next";
import Link from "next/link";
import { useEffect, useState } from "react";
import Head from "next/head";
import Image from "next/image";
import { getSession } from "next-auth/react";
import client from "../../lib/client";
import { QUERIES_TRAVELS } from "../../queries";
import { DashboardPageProps } from "../../types";
import Welcome from "./../../components/ui/Welcome";

const pages = [
  {
    name: "Travel Packages",
    url: "/travel",
    image: "/travel.png",
  },
  {
    name: "Customers",
    url: "/customer",
    image: "/people.png",
  },
  {
    name: "Orders",
    url: "/order",
    image: "/order.png",
  },
];

const Dashboard: NextPage<DashboardPageProps> = ({ travels }) => {
  const [activeImage, setActiveImage] = useState(0);

  useEffect(() => {
    const interval = setInterval(() => {
      setActiveImage((prev) => (prev + 1) % travels.length);
    }, 5000);
    return () => clearInterval(interval);
  }, [travels]);
  return (
    <>
      <Head>
        <title>Dashboard - Travel CMS</title>
      </Head>
      <main>
        <Welcome desc="This is the default page of the Travel CMS." />
        <div className="flex flex-col md:flex-row items-start gap-8 w-full my-12">
          <div className="basis-full md:basis-2/3 flex flex-col gap-4">
            {pages.map((page, index) => (
              <Link key={index} href={page.url}>
                <a className="flex items-center justify-between px-8 py-4 bg-white rounded-lg hover:(scale-102) transform transition-transform duration-300">
                  <Image
                    src={page.image}
                    alt="travel"
                    width={80}
                    height={80}
                    objectFit="contain"
                  />
                  <div className="flex flex-col gap-1 text-right">
                    <div className="text-xl font-semibold">{page.name}</div>
                    <span className="text-sm">
                      Create, Read, Update, and Delete {page.name}
                    </span>
                  </div>
                </a>
              </Link>
            ))}
          </div>
          <Link href={`/travel/${travels[activeImage].id}`}>
            <a className="flex items-end w-full basis-full md:basis-1/3 relative aspect-square bg-white overflow-clip rounded-lg group">
              {travels.map((item, index) => (
                <div key={index}>
                  <Image
                    src={item.attributes.img.data.attributes.url}
                    alt="image"
                    layout="fill"
                    objectFit="cover"
                    className={`${
                      index === activeImage ? "opacity-100" : "opacity-0"
                    } transform transition-all duration-300 group-hover:(scale-110) transform-gpu transition-all duration-300`}
                  />
                  <div className="absolute inset-0 bg-gradient-to-t from-gray-600 via-transparent to-transparent" />
                  <div
                    className={`${
                      index === activeImage ? "flex" : "hidden"
                    } absolute bottom-0 p-4 flex-col text-left text-white gap-2 z-1`}
                  >
                    <div className="text-lg lg:text-2xl">
                      {item.attributes.title}
                    </div>
                    <span className="text-xs lg:text-sm">
                      {item.attributes.description.substring(0, 150)}...
                    </span>
                  </div>
                </div>
              ))}
            </a>
          </Link>
        </div>
      </main>
    </>
  );
};

export default Dashboard;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);
  if (session == null) {
    return {
      redirect: {
        destination: "/auth",
        permanent: true,
      },
    };
  }
  const { data } = await client.query({
    query: QUERIES_TRAVELS,
  });
  return {
    props: { travels: data.travelPackages.data },
  };
};
