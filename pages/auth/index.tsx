import type { NextPage, GetServerSideProps } from "next";
import React, { useEffect, useState } from "react";
import { useRouter } from "next/router";
import { signIn, useSession, getSession } from "next-auth/react";
import toast from "react-hot-toast";
import { register } from "../../services/auth";

const Auth: NextPage = () => {
  const router = useRouter();
  const { data: session } = useSession();

  const initialForm = {
    username: "",
    email: "",
    password: "",
  };

  const [form, setForm] = useState(initialForm);
  const [isRegister, setIsRegister] = useState(false);

  const handleChange = (e: React.ChangeEvent<HTMLInputElement>) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value,
    });
  };

  const handleSubmit = (e: React.FormEvent<HTMLFormElement>) => {
    e.preventDefault();
    const promise = new Promise(async (resolve, reject) => {
      const tryLogin = async (email: string, password: string) => {
        const result = await signIn("credentials", {
          redirect: false,
          email: email,
          password: password,
        });
        if (result.ok) {
          resolve(result);
          router.replace("/dashboard");
        } else {
          reject(result);
        }
      };
      if (isRegister) {
        const response = await register(form);
        await tryLogin(response.user.email, form.password);
      } else {
        await tryLogin(form.email, form.password);
      }
    });
    toast.promise(promise, {
      loading: isRegister ? "Signing up..." : "Signing in...",
      success: isRegister ? "Signed up!" : "Signed in!",
      error: isRegister ? "Failed to Sign up!" : "Failed to Sign in!",
    });
  };

  useEffect(() => {
    if (session) {
      router.replace("/dashboard");
    }
  }, [session]); // eslint-disable-line
  return (
    <div className="fixed inset-0 flex items-center justify-center bg-sky-100">
      <div className="text-center w-468px bg-white px-12 py-8 rounded-lg">
        <h2 className="text-2xl font-semibold mb-8">
          {isRegister ? "Register" : "Sign In"}
        </h2>
        <form
          onSubmit={handleSubmit}
          className="flex flex-col text-left pt-8 gap-4 border-t border-gray-300"
        >
          {isRegister && (
            <div className="w-full">
              <label htmlFor="username" className="block">
                Username
              </label>
              <input
                type="username"
                name="username"
                id="username"
                value={form.username}
                onChange={handleChange}
                className="border border-gray-300 rounded-lg p-2 w-full"
              />
            </div>
          )}
          <div className="w-full">
            <label htmlFor="email" className="block">
              Email
            </label>
            <input
              type="email"
              name="email"
              id="email"
              value={form.email}
              onChange={handleChange}
              className="border border-gray-300 rounded-lg p-2 w-full"
            />
          </div>
          <div className="w-full">
            <label htmlFor="password" className="block">
              Password
            </label>
            <input
              type="password"
              name="password"
              id="password"
              value={form.password}
              onChange={handleChange}
              className="border border-gray-300 rounded-lg p-2 w-full"
            />
          </div>
          <button
            type="submit"
            className="bg-sky-500 py-2 text-center text-white rounded-lg hover:(bg-sky-600 text-gray-200) transition-all duration-200"
          >
            {isRegister ? "Register" : "Sign In"}
          </button>
          <div className="text-sm text-gray-600">
            {isRegister
              ? "Already have an Account? "
              : "Don&apos;t have an Account? "}
            <span
              onClick={() => setIsRegister(!isRegister)}
              className="cursor-pointer text-sky-500 font-semibold"
            >
              {isRegister ? "Sign In" : "Register"}
            </span>
          </div>
        </form>
      </div>
    </div>
  );
};

export default Auth;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);
  if (session) {
    return {
      redirect: {
        destination: "/dashboard",
        permanent: true,
      },
    };
  }
  return {
    props: {},
  };
};
