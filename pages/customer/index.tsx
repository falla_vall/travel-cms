import type { NextPage, GetServerSideProps } from "next";
import { useState } from "react";
import Head from "next/head";
import { getSession } from "next-auth/react";
import { useMutation } from "@apollo/client";
import client from "../../lib/client";
import {
  QUERIES_CUSTOMERS,
  UPDATE_CUSTOMER,
  DELETE_CUSTOMER,
} from "../../queries";
import Table from "../../components/ui/Table";
import CreateModal from "../../components/ui/CreateModal";
import { CustomersPageProps } from "../../types";
import Welcome from "../../components/ui/Welcome";

const Customer: NextPage<CustomersPageProps> = ({ customers }) => {
  const columns = ["id", "name", "email", "phone", "address"];
  const [isOpen, setIsOpen] = useState(false);
  const [data, setData] = useState(customers);
  const [updateCustomer, updOptions] = useMutation(UPDATE_CUSTOMER);
  const [deleteCustomer, delOptions] = useMutation(DELETE_CUSTOMER);
  return (
    <>
      <Head>
        <title>Customer - Travel CMS</title>
      </Head>
      <main>
        <Welcome desc="This page is for CRUD operations on Customers." />
        <button
          onClick={() => setIsOpen(true)}
          className="mt-12 mb-4 px-4 py-2 rounded-lg bg-sky-500 text-white hover:(bg-sky-600 text-gray-200) transition-colors duration-200"
        >
          Create New
        </button>
        <Table
          data={data}
          columns={columns}
          delMutation={[deleteCustomer, delOptions]}
          updMutation={[updateCustomer, updOptions]}
        />
        <CreateModal visState={[isOpen, setIsOpen]} setData={setData} />
      </main>
    </>
  );
};

export default Customer;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);
  if (session == null) {
    return {
      redirect: {
        destination: "/auth",
        permanent: true,
      },
    };
  }
  const { data } = await client.query({
    query: QUERIES_CUSTOMERS,
  });
  return {
    props: { customers: data.customers.data },
  };
};
