import type { NextPage, GetServerSideProps } from "next";
import React, { useEffect } from "react";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { useMutation } from "@apollo/client";
import { useRecoilState } from "recoil";
import toast from "react-hot-toast";
import client from "../../lib/client";
import { loadingState } from "../../atoms";
import { QUERIES_ORDER, DELETE_ORDER } from "../../queries";
import { OrderPageProps } from "../../types";
import Welcome from "../../components/ui/Welcome";

const OrderPage: NextPage<OrderPageProps> = ({ order }) => {
  const router = useRouter();
  const [loading, setLoading] = useRecoilState(loadingState);
  const [deleteOrder, options] = useMutation(DELETE_ORDER);

  const handleDelete = () => {
    const promise = deleteOrder({
      variables: {
        id: order.id,
      },
    }).then(() => router.push("/order"));
    toast.promise(promise, {
      loading: "Deleting...",
      success: "Deleted!",
      error: "Error deleting!",
    });
  };

  const items = [
    {
      name: "Order ID",
      value: order.id,
    },
    {
      name: "Total Price",
      value: new Intl.NumberFormat("en-US", {
        style: "currency",
        currency: "IDR",
      }).format(order.attributes.totalPrice),
    },
    {
      name: "Customer",
      value: order.attributes.customer.data.attributes.name,
    },
    {
      name: "Customer ID",
      value: order.attributes.customer.data.id,
    },
    {
      name: "Created At",
      value: new Date(order.attributes.createdAt).toUTCString(),
    },
  ];

  useEffect(() => {
    if (options.loading) {
      setLoading(true);
    } else {
      setLoading(false);
    }
    if (options.error) {
      console.log(options.error);
    }
  }, [options]); // eslint-disable-line
  return (
    <>
      <Head>
        <title>Order - Travel CMS</title>
      </Head>
      <main>
        <Welcome desc="This page is for CRUD operations on Order." />
        <div className="bg-white rounded-lg px-12 py-8 my-12">
          <div className="flex justify-between items-center">
            <h2 className="text-2xl">Order Details</h2>
            <button
              onClick={() => handleDelete()}
              className="border-2 border-red-500 rounded-lg px-4 py-2 shadow shadow-red-500 text-red-500 hover:(bg-red-500 text-white) transition-colors duration-200"
            >
              Delete
            </button>
          </div>
          <div className="flex pt-8 gap-12">
            <div className="basis-1/2 space-y-6">
              {items.map((item, index) => (
                <div key={index} className="flex justify-between items-center">
                  <div className="text-sm font-semibold">{item.name}</div>
                  <div className="text-sm font-semibold">{item.value}</div>
                </div>
              ))}
            </div>
            <div className="basis-1/2 space-y-2">
              {order.attributes.order_details.data.map((item, index) => (
                <div
                  key={index}
                  className="flex justify-between p-2 border border-gray-300 shadow rounded-lg"
                >
                  <div className="basis-3/4 flex items-center gap-4">
                    <div className="basis-3/4 relative aspect-video bg-white rounded-lg overflow-clip">
                      <Image
                        src={
                          item.attributes.travel_package.data.attributes.img
                            .data.attributes.url
                        }
                        alt="image"
                        layout="fill"
                        objectFit="cover"
                      />
                    </div>
                    <div className="flex flex-col basis-3/4 text-sky-900">
                      <span className="text-xl">
                        {item.attributes.travel_package.data.attributes.title}
                      </span>
                      <span className="text-xs font-light">
                        {new Intl.NumberFormat("en-US", {
                          style: "currency",
                          currency: "IDR",
                        }).format(item.attributes.price)}
                      </span>
                    </div>
                  </div>
                  <div className="flex flex-col items-end gap-2">
                    <span className="text-sm font-light text-gray-400 p-3">
                      #{item.id}
                    </span>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default OrderPage;

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const id = params?.id;
  const { data } = await client.query({
    query: QUERIES_ORDER,
    variables: {
      id,
    },
  });
  return {
    props: {
      order: data.order.data,
    },
  };
};
