import type { NextPage, GetServerSideProps } from "next";
import Head from "next/head";
import { getSession } from "next-auth/react";
import { useMutation } from "@apollo/client";
import client from "../../lib/client";
import { QUERIES_ORDERS, DELETE_ORDER } from "../../queries";
import Table from "../../components/ui/Table";
import { OrdersPageProps } from "../../types";
import Welcome from "../../components/ui/Welcome";

const Order: NextPage<OrdersPageProps> = ({ orders }) => {
  const columns = ["id", "totalPrice", "customer", "createdAt"];
  const [deleteOrder, delOptions] = useMutation(DELETE_ORDER);
  return (
    <>
      <Head>
        <title>Order - Travel CMS</title>
      </Head>
      <main>
        <Welcome desc="This page is for CRUD operations on Orders." />
        <div className="py-12">
          <Table
            data={orders}
            columns={columns}
            delMutation={[deleteOrder, delOptions]}
            disableEdit
          />
        </div>
      </main>
    </>
  );
};

export default Order;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);
  if (session == null) {
    return {
      redirect: {
        destination: "/auth",
        permanent: true,
      },
    };
  }
  const { data } = await client.query({
    query: QUERIES_ORDERS,
  });
  return {
    props: {
      orders: data.orders.data.map((order: any) => {
        return {
          ...order,
          attributes: {
            ...order.attributes,
            totalPrice: new Intl.NumberFormat("en-US", {
              style: "currency",
              currency: "IDR",
            }).format(order.attributes.totalPrice),
            createdAt: new Date(order.attributes.createdAt).toLocaleString(),
            customer: order.attributes.customer.data.attributes.name,
          },
        };
      }),
    },
  };
};
