import type { GetServerSideProps, NextPage } from "next";
import React, { useEffect, useState, useReducer } from "react";
import Head from "next/head";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import { getSession } from "next-auth/react";
import { HiSearchCircle, HiChevronLeft } from "react-icons/hi";
import { MdBookmarkAdded, MdBookmarkAdd } from "react-icons/md";
import client from "../../lib/client";
import { TravelsPageProps } from "../../types";
import { QUERIES_TRAVELS_CUSTOMERS } from "../../queries";
import Sidebar from "../../components/ui/Sidebar";
import Welcome from "../../components/ui/Welcome";

const Travel: NextPage<TravelsPageProps> = ({ travels, customers }) => {
  const router = useRouter();

  const [data, setData] = useState(travels);
  const [search, setSearch] = useState("");
  const [isOpen, setIsOpen] = useState(false);

  const reducer = (state: any, action: any) => {
    switch (action.type) {
      case "ADD_SELECTED":
        return [...state, action.payload];
      case "REMOVE_SELECTED":
        return state.filter((item: any) => item.id !== action.payload.id);
      case "PURGE_SELECTED":
        return [];
      case "UPDATE_SELECTED":
        return state.map((item: any) => {
          if (item.id === action.payload.id) {
            return action.payload;
          }
          return item;
        });
      default:
        return state;
    }
  };
  const [selecteds, dispatch] = useReducer(reducer, []);

  const handleOrder = (
    e: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    item: any
  ) => {
    e.preventDefault();
    if (selecteds.some((selected: any) => selected.id === item.id)) {
      dispatch({ type: "REMOVE_SELECTED", payload: item });
    } else {
      dispatch({ type: "ADD_SELECTED", payload: item });
    }
    setIsOpen(true);
  };

  useEffect(() => {
    if (search) {
      const result = data.filter((item) => {
        return item.attributes.title
          .toLowerCase()
          .includes(search.toLowerCase());
      });
      setData(result);
    } else {
      setData(travels);
    }
  }, [search, data, travels]);
  return (
    <>
      <Head>
        <title>Travel - Travel CMS</title>
      </Head>
      <Sidebar
        visState={[isOpen, setIsOpen]}
        selectedsState={[selecteds, dispatch]}
        customers={customers}
      />
      <button
        onClick={() => setIsOpen(true)}
        className="absolute top-32 right-0 flex items-center bg-white w-8 h-8 rounded-l-full"
      >
        <HiChevronLeft className="text-2xl" />
      </button>
      <main>
        <div className="flex items-center justify-between">
          <div>
            <Welcome desc="This page is for CRUD operations on Travel Packages." />
          </div>
          <div className="flex justify-between items-center relative bg-white rounded-full overflow-clip">
            <input
              type="text"
              name="search"
              id="search"
              value={search}
              onChange={(e) => setSearch(e.target.value)}
              placeholder="Search"
              className="px-4 py-2 bg-inherit outline-none w-full"
            />
            <div className="absolute flex items-center justify-center inset-y-0 right-0 p-2 z-1">
              <HiSearchCircle size={16} className="fill-sky-500" />
            </div>
          </div>
        </div>
        <button
          onClick={() => router.push("/travel/create")}
          className="mt-12 mb-4 px-4 py-2 rounded-lg bg-sky-500 text-white hover:(bg-sky-600 text-gray-200) transition-colors duration-200"
        >
          Create New
        </button>
        {data.length > 0 ? (
          <div className="grid grid-cols-5 gap-8 mb-12">
            {data.map((item, index) => (
              <Link key={index} href={`/travel/${item.id}`}>
                <a className="relative bg-white p-4 rounded-lg hover:(scale-102) transform-gpu transition-all duration-200">
                  <Image
                    src={item.attributes.img.data.attributes.url}
                    alt="travel"
                    width={400}
                    height={225}
                    objectFit="cover"
                    className="rounded-lg"
                  />
                  <div className="text-xl font-semibold py-2">
                    {item.attributes.title}
                  </div>
                  <p className="text-xs">
                    {item.attributes.description.substring(0, 150)}...
                  </p>
                  {selecteds.some(
                    (selected: any) => selected.id === item.id
                  ) ? (
                    <button
                      onClick={(e) => handleOrder(e, item)}
                      className="absolute w-10 h-10 rounded-full top-4 -right-2 bg-sky-600"
                    >
                      <MdBookmarkAdded size={16} className="fill-white" />
                    </button>
                  ) : (
                    <button
                      onClick={(e) => handleOrder(e, item)}
                      className="absolute w-10 h-10 rounded-full top-4 -right-2 bg-sky-500"
                    >
                      <MdBookmarkAdd size={16} className="fill-white" />
                    </button>
                  )}
                </a>
              </Link>
            ))}
          </div>
        ) : (
          <div className="py-12 text-center">
            <h1 className="text-7xl py-8">{"(_　_)。゜zｚＺ"}</h1>
            <p className="text-xl">No Travel Packages Found</p>
          </div>
        )}
      </main>
    </>
  );
};

export default Travel;

export const getServerSideProps: GetServerSideProps = async (context) => {
  const session = await getSession(context);
  if (session == null) {
    return {
      redirect: {
        destination: "/auth",
        permanent: true,
      },
    };
  }
  const { data } = await client.query({
    query: QUERIES_TRAVELS_CUSTOMERS,
  });
  return {
    props: {
      travels: data.travelPackages.data,
      customers: data.customers.data,
    },
  };
};
