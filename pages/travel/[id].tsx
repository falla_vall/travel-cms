import type { NextPage, GetServerSideProps } from "next";
import { useEffect, useState, ChangeEvent, ChangeEventHandler } from "react";
import Head from "next/head";
import Image from "next/image";
import { useRouter } from "next/router";
import { useMutation } from "@apollo/client";
import toast from "react-hot-toast";
import { useRecoilState } from "recoil";
import { IoImagesOutline } from "react-icons/io5";
import client from "../../lib/client";
import FilesModal from "../../components/ui/FilesModal";
import { loadingState } from "../../atoms";
import { FileProp, TravelPageProps } from "../../types";
import {
  QUERIES_TRAVEL_FILES,
  QUERIES_FILES,
  CREATE_TRAVEL,
  UPDATE_TRAVEL,
  DELETE_TRAVEL,
} from "../../queries";
import Welcome from "../../components/ui/Welcome";

const Customer: NextPage<TravelPageProps> = ({ travel, files }) => {
  const router = useRouter();

  const [preview, setPreview] = useState<FileProp>();
  const [data, setData] = useState(
    travel || {
      id: 0,
      attributes: {
        title: "",
        description: "",
        price: 0,
        img: {
          data: {
            id: 0,
            attributes: {
              name: "",
              url: "/placeholder.png",
            },
          },
        },
      },
    }
  );
  const [isOpen, setIsOpen] = useState(false);
  const [loading, setLoading] = useRecoilState(loadingState);

  const [createTravel, creOptions] = useMutation(CREATE_TRAVEL);
  const [updateTravel, updOptions] = useMutation(UPDATE_TRAVEL);
  const [deleteTravel, delOptions] = useMutation(DELETE_TRAVEL);

  const fields = [
    {
      name: "Title",
      value: data.attributes.title,
    },
    {
      name: "Price",
      value: data.attributes.price,
    },
    {
      name: "Description",
      value: data.attributes.description,
    },
  ];

  const handleSubmit = () => {
    if (data.id > 0) {
      const promise = updateTravel({
        variables: {
          id: data.id,
          title: data.attributes.title,
          price: +data.attributes.price,
          description: data.attributes.description,
          img: preview?.id,
        },
      });

      toast.promise(promise, {
        loading: "Updating travel...",
        success: "Travel updated successfully",
        error: "Error updating travel",
      });
    } else {
      const promise = createTravel({
        variables: {
          title: data.attributes.title,
          price: +data.attributes.price,
          description: data.attributes.description,
          img: preview.id,
        },
      });

      toast.promise(promise, {
        loading: "Creating travel...",
        success: "Travel created successfully",
        error: "Error creating travel",
      });
    }
  };

  const handleChange: ChangeEventHandler = (
    e: ChangeEvent<HTMLInputElement>
  ) => {
    setData({
      ...data,
      attributes: {
        ...data.attributes,
        [e.target.name.toLowerCase()]: e.target.value,
      },
    });
  };

  const handleDelete = () => {
    const promise = deleteTravel({
      variables: {
        id: data.id,
      },
    }).then(() => router.push("/travel"));
    toast.promise(promise, {
      loading: "Deleting travel...",
      success: "Travel deleted successfully",
      error: "Error deleting travel",
    });
  };

  useEffect(() => {
    if (updOptions.loading || creOptions.loading || delOptions.loading) {
      setLoading(true);
    } else {
      setLoading(false);
    }
    if (updOptions.error) {
      console.log({ error: updOptions.error });
    }
    if (creOptions.error) {
      console.log({ error: creOptions.error });
    }
  }, [updOptions, creOptions, delOptions]); // eslint-disable-line

  return (
    <>
      <Head>
        <title>{travel?.attributes.title || "Create New"} - Travel CMS</title>
      </Head>
      <FilesModal
        visState={[isOpen, setIsOpen]}
        files={files}
        previewState={[preview, setPreview]}
      />
      <main>
        <Welcome desc="This page is for CRUD operations on Travel Packages." />
        <div className="flex justify-center">
          <div className="w-2/3 flex flex-col gap-12 p-12 my-12 bg-white rounded-lg">
            <button
              onClick={() => setIsOpen(true)}
              className="relative aspect-video rounded-lg overflow-clip group cursor-pointer"
            >
              <Image
                src={
                  preview?.attributes.url ??
                  data.attributes.img.data.attributes.url
                }
                alt="image"
                layout="fill"
                objectFit="cover"
              />
              <div className="flex justify-center items-center opacity-0 group-hover:(opacity-100) transform-gpu transition-opacity duration-200 absolute inset-0 bg-[rgba(0,0,0,.4)]">
                <IoImagesOutline size={64} className="text-white" />
              </div>
            </button>
            {fields.map((field, index) => (
              <label
                key={index}
                htmlFor={field.name.toLowerCase()}
                className="flex flex-col"
              >
                <span>{field.name}</span>
                {field.name === "Description" ? (
                  <textarea
                    rows={4}
                    name={field.name.toLowerCase()}
                    id={field.name.toLowerCase()}
                    value={field.value}
                    onChange={handleChange}
                    className="border border-gray-300 rounded-lg p-2"
                  />
                ) : (
                  <input
                    type="text"
                    name={field.name.toLowerCase()}
                    id={field.name.toLowerCase()}
                    value={field.value}
                    onChange={handleChange}
                    className="border border-gray-300 rounded-lg p-2"
                  />
                )}
              </label>
            ))}
            <div className="flex gap-4 justify-end items-center">
              {data.id > 0 && (
                <button
                  onClick={() => handleDelete()}
                  className="border-2 border-red-500 shadow shadow-red-500 text-red-500 px-12 py-2 rounded-lg text-white hover:(bg-red-500 text-gray-200) transition-all duration-200"
                >
                  Delete
                </button>
              )}
              <button
                onClick={() => handleSubmit()}
                className="bg-sky-500 px-12 py-2 rounded-lg text-white hover:(bg-sky-600 text-gray-200) transition-all duration-200"
              >
                {data.id > 0 ? "Update" : "Create"}
              </button>
            </div>
          </div>
        </div>
      </main>
    </>
  );
};

export default Customer;

export const getServerSideProps: GetServerSideProps = async ({ params }) => {
  const id = params?.id;
  if (id === "create") {
    const { data } = await client.query({
      query: QUERIES_FILES,
    });
    return {
      props: {
        files: data.uploadFiles.data,
      },
    };
  }
  const { data } = await client.query({
    query: QUERIES_TRAVEL_FILES,
    variables: { id },
  });
  return {
    props: { travel: data.travelPackage.data, files: data.uploadFiles.data },
  };
};
