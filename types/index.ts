import React, { Dispatch, SetStateAction } from "react";
import {
  MutationResult,
  MutationFunctionOptions,
  OperationVariables,
  DefaultContext,
  ApolloCache,
} from "@apollo/client";

export interface TravelProp {
  id: number;
  attributes: {
    title: string;
    description: string;
    price: number;
    img: {
      data: {
        id: number;
        attributes: {
          name: string;
          url: string;
        };
      };
    };
  };
}

export interface CustomerProp {
  id: number;
  attributes: {
    name: string;
    email: string;
    phone: string;
    address: string;
  };
}

export interface OrderProp {
  id: number;
  attributes: {
    totalPrice: string;
    createdAt: string;
    customer: string;
  };
}

export interface OrderFullProp {
  id: number;
  attributes: {
    totalPrice: number;
    createdAt: string;
    customer: {
      data: {
        id: number;
        attributes: {
          name: string;
        };
      };
    };
    order_details: {
      data: [
        {
          id: number;
          attributes: {
            price: number;
            travel_package: {
              data: {
                id: number;
                attributes: {
                  title: string;
                  img: {
                    data: {
                      attributes: {
                        url: string;
                      };
                    };
                  };
                };
              };
            };
          };
        }
      ];
    };
  };
}

export interface FileProp {
  id: number;
  attributes: {
    name: string;
    url: string;
  };
}

export interface SignInService {
  email: string;
  password: string;
}

export interface RegisterService {
  username: string;
  email: string;
  password: string;
}

export type ChildrenProps = {
  children: React.ReactNode;
};

export type DashboardPageProps = {
  travels: Array<TravelProp>;
};

export type TravelsPageProps = {
  travels: Array<TravelProp>;
  customers: Array<CustomerProp>;
};

export type TravelPageProps = {
  travel: TravelProp;
  files: Array<FileProp>;
};

export type CustomersPageProps = {
  customers: Array<CustomerProp>;
};

export type OrdersPageProps = {
  orders: Array<OrderProp>;
};

export type OrderPageProps = {
  order: OrderFullProp;
};

export type ModalProps = {
  children: React.ReactNode;
  visState: [boolean, (state: boolean) => void];
  position?: string;
};

export type FilesModalProps = {
  visState: [boolean, (state: boolean) => void];
  previewState: [FileProp, Dispatch<SetStateAction<FileProp>>];
  files: Array<FileProp>;
};

export type CreateModalProps = {
  visState: [boolean, (state: boolean) => void];
  setData: Dispatch<SetStateAction<CustomerProp[]>>;
};

export type SidebarProps = {
  visState: [boolean, (state: boolean) => void];
  selectedsState: [Array<any>, React.Dispatch<any>];
  customers: Array<CustomerProp>;
};

export type LoadingProps = {
  visState: [boolean, (state: boolean) => void];
};

export type DropUpProps = {
  selectedState: [any, React.SetStateAction<any>];
  options: Array<any>;
};

export type WelcomeProps = {
  desc: string;
};

export type TableProps = {
  data: Array<any>;
  columns: Array<string>;
  delMutation: [
    (
      options?: MutationFunctionOptions<
        any,
        OperationVariables,
        DefaultContext,
        ApolloCache<any>
      >
    ) => Promise<any>,
    MutationResult
  ];
  updMutation?: [
    (
      options?: MutationFunctionOptions<
        any,
        OperationVariables,
        DefaultContext,
        ApolloCache<any>
      >
    ) => Promise<any>,
    MutationResult
  ];
  disableEdit?: boolean;
};
