import client from "../lib/client";
import { LOGIN, REGISTER } from "../queries";
import { SignInService, RegisterService } from "../types";

export const signIn = async ({ email, password }: SignInService) => {
  const { data } = await client.mutate({
    mutation: LOGIN,
    variables: { email, password },
  });
  return data.login;
};

export const register = async ({
  username,
  email,
  password,
}: RegisterService) => {
  const { data } = await client.mutate({
    mutation: REGISTER,
    variables: { username, email, password },
  });
  return data.register;
};
