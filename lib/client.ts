import { ApolloClient, InMemoryCache, DefaultOptions } from "@apollo/client";
import { createUploadLink } from "apollo-upload-client";

const defaultOptions: DefaultOptions = {
  watchQuery: {
    fetchPolicy: "no-cache",
    errorPolicy: "ignore",
  },
  query: {
    fetchPolicy: "no-cache",
    errorPolicy: "all",
  },
};

export default new ApolloClient({
  cache: new InMemoryCache(),
  link: createUploadLink({
    uri: "https://travel-cuy.herokuapp.com/graphql/",
    headers: {
      "Content-Type": "application/json",
      Authorization:
        "Bearer 9b7fa2e6cbdbbcae0d8dcf2f49576c29071ed6df7a6c131892b1debce90356286df2d1b9922a861f7178bad2953483bbd6b4fb75276ed6d2d03b7730d9ee8a76264be888f39f94be03841b4e403bc20d3b46d8e23af09da78678eca66d37ce12d4355d97a8e95ed4ecd2d8a886b76c06efd92cf9e86265bae5b512d7da254ad1",
    },
  }),
  defaultOptions,
});
